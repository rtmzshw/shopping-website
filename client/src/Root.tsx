import { WithStyles } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import React, { Children, FunctionComponent, useState } from 'react';
import { RouterProvider } from 'react-router-dom';
import { styles } from './App.styles';
import { createViews } from './App.views';
import AppBar from './components/AppBar/AppBar';
import { Product } from './containers/ProductsList/ProductsList.types';

interface RootProps extends WithStyles<typeof styles> {
    children: JSX.Element;
}

const Root: FunctionComponent<RootProps> = props => {
    const { classes, children } = props;

    return (
        <div className={classes.root}>
            <AppBar />
            <div className={classes.products}>
                {children}
            </div>
        </div>
    )
}

export default withStyles(styles)(Root);