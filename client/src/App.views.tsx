import { createBrowserRouter } from "react-router-dom";
import ProductPage from "./containers/productPage/productPage";
import ProductsList, { ProductListProps } from "./containers/ProductsList/ProductsList";
import ShoppingCart, { ShoppingCartProps } from "./containers/ShoppingCart/ShoppingCart";
import React from 'react';
import Root from "./Root";

export const    createViews = (productListProps: Omit<ProductListProps, 'classes'>, shoppingCartProps: Omit<ShoppingCartProps, 'classes'>) => createBrowserRouter([
    {
        path: "/",
        element: <Root children={<ProductsList  {...productListProps} />} />,
    },
    {
        path: "product/:productId",
        element: <Root children={<ProductPage />} />,
    },
    {
        path: "cart/",
        element: <Root children={<ShoppingCart {...shoppingCartProps} />} />,
    }
]);