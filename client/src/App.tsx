import { WithStyles } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import React, { FunctionComponent, useState } from 'react';
import { RouterProvider } from 'react-router-dom';
import { styles } from './App.styles';
import { createViews } from './App.views';
import { Product } from './containers/ProductsList/ProductsList.types';

interface AppProps extends WithStyles<typeof styles> { }

const App: FunctionComponent<AppProps> = props => {
  const [shoppingList, setShoppingList] = useState<Product[]>([]);

  const emptyCart = () => setShoppingList([])
  const router = createViews({ setShoppingList }, { products: shoppingList, emptyCart });

  return (
    <RouterProvider router={router} />
  )
}

export default withStyles(styles)(App);