import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            height: '100vh',
            width: '100vw',
            backgroundColor: "#f0f3f9"
        },
        products: {
            height: '95vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    })