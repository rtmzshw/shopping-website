import { IconButton, WithStyles } from "@material-ui/core";
import Paper from '@material-ui/core/Paper';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import { withStyles } from "@material-ui/styles";
import React, { SetStateAction, Dispatch, FunctionComponent } from "react";
import { Product } from "../../containers/ProductsList/ProductsList.types";
import { styles } from "./ProductCard.styles";

interface ProductCardProps extends WithStyles<typeof styles> {
    product: Product;
    setShoppingList?: Dispatch<SetStateAction<Product[]>>;
    addToCart?: boolean;
}

const ProductCard: FunctionComponent<ProductCardProps> = ({ addToCart = true, ...props }) => {
    const { classes, product, setShoppingList } = props;
    const { name, price, image } = product;

    const addToShoppingList = (newProduct: Product) => setShoppingList ? setShoppingList(prevList => prevList.concat(newProduct)) : {}

    return (
        <Paper className={classes.paper}>
            <span className={classes.productInfo}>
                <img className={classes.img} src={image} height="80px" />
                <span className={classes.text}>
                    {name} <br /> {`₪${price}`}
                </span>
            </span>
            {addToCart &&
                <IconButton color="primary" style={{ padding: '20px' }} onClick={event => addToShoppingList(product)}>
                    <AddShoppingCartIcon />
                </IconButton>
            }
        </Paper>
    )
};

export default withStyles(styles)(ProductCard);