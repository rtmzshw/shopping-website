import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        paper: {
            display: 'flex',
            alignItems: 'center',
            justifyContent:"space-between",
            boxShadow: 'none',
            height: '100%',
            width: '100%'
        },
        text: {
            direction: 'rtl'
        },
        img: {
            padding: '5px'
        },
        productInfo: {
            display: 'flex',
            alignItems: 'center',
            padding: '10px'
        }
    })