import { WithStyles, AppBar as MuiAppBar, IconButton, Toolbar } from "@material-ui/core";
import { Link, useNavigate } from "react-router-dom";
import { withStyles } from "@material-ui/styles";
import React, { FunctionComponent } from "react";
import { styles } from "./AppBar.styles";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import StorefrontIcon from '@material-ui/icons/Storefront';

interface AppBarProps extends WithStyles<typeof styles> {
}

const AppBar: FunctionComponent<AppBarProps> = props => {
    const { classes } = props;
    const navigate = useNavigate();

    return (
        <MuiAppBar position="static" className={classes.root}>
            <Toolbar className={classes.toolBar}>
                סופר-סטאר
                <span>
                    <IconButton style={{ color: 'white' }} onClick={() => navigate(-1)} >
                        <StorefrontIcon />
                    </IconButton>
                    <Link to={`cart`} style={{ textDecoration: "none" }}>
                        <IconButton style={{ color: 'white' }}>
                            <ShoppingCartIcon />
                        </IconButton>
                    </Link>
                </span>
            </Toolbar>
        </MuiAppBar >
    )
};

export default withStyles(styles)(AppBar);