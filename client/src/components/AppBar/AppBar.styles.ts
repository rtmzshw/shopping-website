import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            height: '5vh',
            paddingRight: '20px',
            justifyContent: 'center'
        },
        toolBar: {
            minHeight: 'unset', 
            justifyContent: 'space-between'
        }
    })