import { createStyles, FormHelperText, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            display: "flex",
            alignItems: "center",
            height: "98vh"
        },
        details: {
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            padding: "0px 96px"
        },
        brandName: {
            fontWeight: "bold",
            fontSize: "24px",
            textAlign:"center"
        }
    })