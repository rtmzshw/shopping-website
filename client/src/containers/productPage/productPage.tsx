import { WithStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import React from "react";
import { FunctionComponent } from "react";
import { useParams } from "react-router-dom";
import { styles } from "./productPage.styles";

interface ProductPageProps extends WithStyles<typeof styles> { }

const ProductPage: FunctionComponent<ProductPageProps> = props => {
    const { classes } = props;
    let { productId } = useParams();
    // const product = PRODUCTS.find(p => p.id === productId)

    // return product ? (
    //     <div className={classes.root}>
    //         <img src={product.additionImage} style={{height:"90vh", width:"50vw"}} />
    //         <div className={classes.details}>
    //             <div>
    //                 {product.description}
    //             </div>
    //             <div style={{height: "48px"}}></div>
    //             <div className={classes.brandName}>
    //                 {product.storeName}
    //             </div>
    //         </div>
    //     </div>
    // )
    //     : <div></div>

    return (<div></div>)
};

export default withStyles(styles)(ProductPage);