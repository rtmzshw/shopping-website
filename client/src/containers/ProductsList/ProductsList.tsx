import { WithStyles } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import axios from 'axios';
import React, { Dispatch, FunctionComponent, SetStateAction, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { LOCALHOST_SERVER } from "../../App.consts";
import ProductCard from "../../components/ProductCard/ProductCard";
import { styles } from "./ProductList.styles";
import { Product } from "./ProductsList.types";

export interface ProductListProps extends WithStyles<typeof styles> {
    setShoppingList: Dispatch<SetStateAction<Product[]>>
}

const ProductList: FunctionComponent<ProductListProps> = props => {
    const { classes, setShoppingList } = props;
    const [products, setProducts] = useState<Product[]>([]);

    useEffect(() => {
        const getProductsList = async () => {
            const newProducts = (await axios.get<Product[]>(`${LOCALHOST_SERVER}/products`)).data;
            setProducts(newProducts);
        }

        getProductsList();
    }, [])
    
    return (
        <div className={classes.root}>
            {products.map(product => {
                return (
                    // <Link to={`product/${product.name}`} className={classes.product} style={{ textDecoration: "none" }} key={product._id}>
                        <div className={classes.product}  >
                            <ProductCard {...{ product, setShoppingList }} />
                        </div>
                    // </Link>

                )
            })}
        </div>
    )
};

export default withStyles(styles)(ProductList);