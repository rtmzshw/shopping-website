import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            height: '98%',
            padding: '20px',
            display: 'flex',
            flexWrap: 'wrap',
            overflowY: 'auto'
        },
        product: {
            padding: '5px',
            height: '20%',
            width: '15%'
        }
        
    })