export interface Product {
    _id: string;
    name: string;
    price: number;
    image: string;
    description: string;
    storeName: string;
    additionImage: string;
}