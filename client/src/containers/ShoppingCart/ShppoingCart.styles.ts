import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            height: '98%',
            width: '55%',
            padding: '20px',
            display: 'flex',
            flexDirection: 'column'
        },
        product: {
            padding: '10px'
        },
        products: {
            height: '70%',
            overflowY: 'auto'
        },
        userInfo: {
            height: '8%',
            display: 'flex'
        },
        sendBtn: {
            width: '10%',
            alignSelf: 'end',
            fontFamily: 'inherit'
        },
        icon: {
            transform: 'rotate(180deg)',
            marginRight: '7px'
        },
        bottom: {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        }
    })