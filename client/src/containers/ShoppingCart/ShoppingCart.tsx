import { Button, TextField, WithStyles } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import React, { ChangeEvent, FunctionComponent, useState } from 'react';
import ProductCard from '../../components/ProductCard/ProductCard';
import { Product } from '../ProductsList/ProductsList.types';
import { styles } from './ShppoingCart.styles';
import SendIcon from '@material-ui/icons/Send';
import { LOCALHOST_SERVER } from '../../App.consts';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
interface User {
    name: string;
    mail: string;
}

export interface ShoppingCartProps extends WithStyles<typeof styles> {
    products: Product[],
    emptyCart: () => void
}

const App: FunctionComponent<ShoppingCartProps> = props => {
    const { classes, products, emptyCart } = props;
    const [user, setUser] = useState<User>({ name: '', mail: '' });
    const total = products.reduce((prevPrice, { price }, inx) => prevPrice += price, 0);
    const navigate = useNavigate();

    const updateUser = (prop: keyof User) => (event: ChangeEvent<any>) => {
        const { target: { value } } = event;
        setUser(user => ({ ...user, [prop]: value }));
    }

    const sendData = async () => {
        await axios.post<Product[]>(`${LOCALHOST_SERVER}/products`, { products, total, user })
        emptyCart()
    };

    return (
        <div className={classes.root}>
            <h2>עגלת קניות</h2>
            <div className={classes.products}>
                {products.map(product => {
                    return (
                        <div className={classes.product} key={product._id}>
                            <ProductCard {...{ product, addToCart: false }} />
                        </div>
                    )
                })}
            </div>
            <h2>פרטי משתמש</h2>
            <div className={classes.userInfo}>
                <TextField id="outlined-basic" label="שם מלא" variant="outlined"
                    value={user.name}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    onChange={updateUser('name')}
                />
                <TextField id="outlined-basic" label="מייל" variant="outlined"
                    value={user.mail}
                    InputLabelProps={{
                        shrink: true,
                    }}
                    style={{ marginRight: '60px' }}
                    onChange={updateUser('mail')}
                />
            </div>
            <span className={classes.bottom}>
                {`סך הכל: ₪${total}`}
                <Button
                    variant="contained"
                    color="primary"
                    endIcon={<SendIcon className={classes.icon} />}
                    className={classes.sendBtn}
                    onClick={e => sendData()}
                >
                    שלח
                </Button>
            </span>
        </div>
    )
}

export default withStyles(styles)(App);