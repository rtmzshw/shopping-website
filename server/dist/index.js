"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongo_1 = require("./mongo");
const products_schema_1 = require("./products/products.schema");
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = require("mongoose");
const app = (0, express_1.default)();
const port = 4200;
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.get('/products', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const products = yield products_schema_1.productsModel.find({});
    res.json(products);
}));
app.post('/products', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const order = Object.assign(Object.assign({}, req.body.user), { products: req.body.products, total: req.body.total });
    yield mongoose_1.connection.collection("orders").insertOne(order);
    res.sendStatus(200);
}));
const start = () => __awaiter(void 0, void 0, void 0, function* () {
    yield (0, mongo_1.connectMongo)();
    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`);
    });
});
start();
