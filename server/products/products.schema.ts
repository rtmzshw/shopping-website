import mongoose from "mongoose"

const productsSchema = new mongoose.Schema({
    name: String,
    price: Number,
    image: String,
    description: String,
    storeName: String,
    additionImage: String
});


export const productsModel = mongoose.model('products', productsSchema);

