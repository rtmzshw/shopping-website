import express, { Request, Response } from 'express'
import { connectMongo } from './mongo'
import { productsModel } from './products/products.schema'
import cors from 'cors'
import { updateLanguageServiceSourceFile } from 'typescript'
import { connection } from 'mongoose'

const app = express()
const port = 4200

app.use(cors());
app.use(express.json())

app.get('/products', async (req: Request, res: Response) => {
    const products = await productsModel.find({})
    res.json(products)
})
app.post('/products', async (req: Request, res: Response) => {
    const order = {...req.body.user, products: req.body.products, total: req.body.total}
    await connection.collection("orders").insertOne(order)
    res.sendStatus(200)
})

const start = async () => {
    await connectMongo()

    app.listen(port, () => {
        console.log(`Example app listening on port ${port}`)
    })
}

start()